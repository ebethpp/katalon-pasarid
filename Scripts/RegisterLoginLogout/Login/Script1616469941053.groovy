import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.pasarid)

WebUI.click(findTestObject('Daftar/Page_Pasar.id/a_OK'), FailureHandling.OPTIONAL)

WebUI.acceptAlert(FailureHandling.OPTIONAL)

WebUI.sendKeys(findTestObject('Daftar/Page_Pasar.id/a_OK'), Keys.chord(Keys.ENTER), FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/Daftar/Page_Pasar.id/imgDaftar'))

WebUI.click(findTestObject('Object Repository/Daftar/Page_Pasar.id/div_Masuk'))

WebUI.delay(5)

WebUI.setText(findTestObject('Login/Page_Pasar.id/input_Alamat Email_form-control'), 'test321@mailinator.com')

WebUI.setText(findTestObject('Login/Page_Pasar.id/input_Password_form-control'), 'qwerty@123A')

WebUI.click(findTestObject('Login/Page_Pasar.id/button_Login'))

WebUI.click(findTestObject('Daftar/Page_Pasar.id/a_OK'), FailureHandling.OPTIONAL)

WebUI.acceptAlert(FailureHandling.OPTIONAL)

WebUI.sendKeys(findTestObject('Daftar/Page_Pasar.id/a_OK'), Keys.chord(Keys.ENTER), FailureHandling.OPTIONAL)

WebUI.delay(3)

