import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('RegisterLoginLogout/Login'), [('emaillogin') : '', ('passwordlogin') : ''], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Daftar/Page_Pasar.id/imgDaftar'))

WebUI.click(findTestObject('Object Repository/Edit Profile/Page_Pasar.id/span_Edit Profil'))

WebUI.click(findTestObject('Object Repository/Edit Profile/Page_Pasar.id/span_Ubah Data Diri Anda Disini'))

WebUI.click(findTestObject('Object Repository/Edit Profile/Page_Pasar.id/button_Edit'))

WebUI.setText(findTestObject('Edit Profile/Page_Pasar.id/input_Nama Depan_form-control'), 'Test')

WebUI.setText(findTestObject('Edit Profile/Page_Pasar.id/input_Nama Belakang_form-control'), 'ABCDEF')

WebUI.setText(findTestObject('Edit Profile/Page_Pasar.id/input_No. Telepon_form-control form-control'), '')

WebUI.setText(findTestObject('Edit Profile/Page_Pasar.id/textarea_Alamat Pembeli_form-control'), 'Jalan Raya')

WebUI.click(findTestObject('Object Repository/Edit Profile/Page_Pasar.id/button_Simpan'))

WebUI.click(findTestObject('Object Repository/Edit Profile/Page_Pasar.id/a_OK'))

WebUI.callTestCase(findTestCase('RegisterLoginLogout/Logout'), [:], FailureHandling.STOP_ON_FAILURE)

