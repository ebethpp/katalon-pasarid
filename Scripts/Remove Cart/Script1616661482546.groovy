import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('RegisterLoginLogout/Login'), [('emaillogin') : '', ('passwordlogin') : ''], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Belanja dan Checkout/Page_Pasar.id/input_Indonesia_rbt-input-main form-control_7078ab'), 
    'jatijajar')

WebUI.click(findTestObject('Object Repository/Belanja dan Checkout/Page_Pasar.id/span_Pedagang Jatijajar'))

WebUI.click(findTestObject('Object Repository/Belanja dan Checkout/Page_Pasar.id/img'))

WebUI.click(findTestObject('Object Repository/Belanja dan Checkout/Page_Pasar.id/a_Lihat Toko'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_menambahkan'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_menambahkan'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_menambahkan'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_mengurangi'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_tmbhkeranjang'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_OK'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/imgCart'))

WebUI.delay(5)

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_mengurangiCart'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_mengurangiCart'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/btn_mengurangiCart'))

WebUI.click(findTestObject('Belanja dan Checkout/Page_Pasar.id/Ya_hapusproduk'))

