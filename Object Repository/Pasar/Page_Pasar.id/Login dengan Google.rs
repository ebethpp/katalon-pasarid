<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Login dengan Google</name>
   <tag></tag>
   <elementGuidId>83bb432c-3ff8-4f06-9336-c947e4a9d843</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@role = 'generic' and @class = 'svg-inline--fa fa-google fa-w-16' and @data-icon = 'google']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>generic</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-inline--fa fa-google fa-w-16</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>google</value>
   </webElementProperties>
</WebElementEntity>
