Feature: Daftar

  Scenario Outline: 
    Given I navigated to Pasar.id page
    When I click signin icon
    Then I click masuk button
    Then I navigated to login screen
    Then I click on Daftar button
    Then I navigated to register screen
    Then I enters nama
    Then I enters alamatemail
    Then I enters notelephone
    Then I enters password
    Then I enters konfirmasipassword

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |