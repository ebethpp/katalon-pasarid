<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login Logout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>33b050f9-92d5-4927-be35-1be3da3245b0</testSuiteGuid>
   <testCaseLink>
      <guid>abc10fc2-2ca3-4751-b18a-c32732a98a74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegisterLoginLogout/Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7e8ce6cb-1ba8-4f8e-a1a7-0af6426c8bc3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9e57ecc0-c62c-499c-8ec6-1a62e679bee0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ccd9eb59-deef-47a2-9ea3-eb920ab5cbf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RegisterLoginLogout/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
